# Copyright (c) IRT Saint Exupery.
#
# This source code is licensed under the MIT license.
import libcst as cst
import libcst.matchers as m
import pytest
from libcst.codemod import CodemodTest

from bump_gemseo.codemods import renaming_definitions

renaming_definitions.ATTR_TO_ATTR = {
    "attr1": "b1",
    "attr2": "b2.c1",
    # WIP
    "attr3.a4": "b3",
}

renaming_definitions.METHOD_TO_METHOD = {
    "method1": "b1",
    "method2": "b2.c1",
    # WIP
    "method3.a4": "b3",
}

renaming_definitions.METHOD_TO_ATTR = {"method4": "attr"}

renaming_definitions.METHOD_WITH_ARGS_TO_ATTR = {
    "method5": {
        "args": [m.Arg(keyword=m.Name("arg"), value=m.Name("value"))],
        "attr": "attr",
    }
}

renaming_definitions.METHOD_ARGS_TO_ARGS = {
    "method6": {
        "old_arg1": "new_arg1",
        "old_arg2": "new_arg2",
    },
}

from bump_gemseo.codemods.attributes_renamer import AttributesRenamer  # noqa: E402


class TestAttributesRenamer(CodemodTest):
    TRANSFORM = AttributesRenamer
    maxDiff = None

    def test_method_with_args_to_attr(self) -> None:
        before = """
        x.method5(arg=value)
        """
        after = """
        x.attr
        """
        self.assertCodemod(before, after)

    def test_method_with_args_to_attr_noop(self) -> None:
        code = """
        x.method5
        x.method5(arg)
        x.method5(arg=foo)
        """
        self.assertCodemod(code, code)

    def test_method_to_attr(self) -> None:
        before = """
        x.method4()
        """
        after = """
        x.attr
        """
        self.assertCodemod(before, after)

    def test_method_to_attr_noop(self) -> None:
        code = """
        x.method4
        """
        self.assertCodemod(code, code)

    def test_method_args_to_args(self) -> None:
        before = """
        x.method6(old_arg1=0)
        x.method6(x,old_arg1=0)
        x.method6(x,old_arg1=0,x=0)
        x.method6(old_arg1=0,x=0)
        x.method6(old_arg1=0,old_arg2=0)
        x.method6(x,old_arg1=0,x=0,old_arg2=0)
        x.method6(old_arg1=0,x=0,old_arg2=0)
        """
        after = """
        x.method6(new_arg1=0)
        x.method6(x,new_arg1=0)
        x.method6(x,new_arg1=0,x=0)
        x.method6(new_arg1=0,x=0)
        x.method6(new_arg1=0,new_arg2=0)
        x.method6(x,new_arg1=0,x=0,new_arg2=0)
        x.method6(new_arg1=0,x=0,new_arg2=0)
        """
        self.assertCodemod(before, after)

    def test_method_args_to_args_noop(self) -> None:
        code = """
        x.method6(old_arg1)
        method6(old_arg1=0)
        method6(x,old_arg1=0)
        method6(x,old_arg1=0,x=0)
        method6(old_arg1=0,x=0)
        method6
        """
        self.assertCodemod(code, code)

    def test_method_params_to_params(self) -> None:
        before = """
        def method6(old_arg1=0): pass
        def method6(x,old_arg1=0): pass
        def method6(x,old_arg1=0,x=0): pass
        def method6(old_arg1=0,x=0): pass
        def method6(old_arg1=0,old_arg2=0): pass
        def method6(x,old_arg1=0,x=0,old_arg2=0): pass
        def method6(old_arg1=0,x=0,old_arg2=0): pass
        """
        after = """
        def method6(new_arg1=0): pass
        def method6(x,new_arg1=0): pass
        def method6(x,new_arg1=0,x=0): pass
        def method6(new_arg1=0,x=0): pass
        def method6(new_arg1=0,new_arg2=0): pass
        def method6(x,new_arg1=0,x=0,new_arg2=0): pass
        def method6(new_arg1=0,x=0,new_arg2=0): pass
        """
        self.assertCodemod(before, after)

    def test_attr_to_attr(self) -> None:
        before = """
        x.attr1
        x.attr1.y
        x.y.attr1

        x.attr2
        x.attr2.y
        x.y.attr2
        """
        after = """
        x.b1
        x.b1.y
        x.y.b1

        x.b2.c1
        x.b2.c1.y
        x.y.b2.c1
        """
        self.assertCodemod(before, after)

    def test_attr_to_attr_noop(self) -> None:
        code = """
        attr1
        attr1()
        attr1.x
        attr2
        attr2()
        attr2.x
        attr3.a4
        """
        self.assertCodemod(code, code)

    def test_method_to_method(self) -> None:
        before = """
        x.method1()
        x.method1(arg)
        x.y.method1()

        x.method2()
        (x.method2())
        x.y.method2()
        """
        after = """
        x.b1()
        x.b1(arg)
        x.y.b1()

        x.b2.c1()
        (x.b2.c1())
        x.y.b2.c1()
        """
        self.assertCodemod(before, after)

    def test_method_to_method_noop(self) -> None:
        code = """
        method1()
        x.method1.y()
        method2()
        x.method2.y()
        """
        self.assertCodemod(code, code)

    def test_method_def(self) -> None:
        before = """
        def _run(self): pass
        """
        after = """
        def _run(self, input_data): pass
        """
        self.assertCodemod(before, after)
