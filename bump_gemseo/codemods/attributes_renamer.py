# Copyright (c) IRT Saint Exupery.
#
# This source code is licensed under the MIT license.
from __future__ import annotations

from functools import singledispatchmethod
from typing import Iterable

import libcst as cst
import libcst.matchers as m
from libcst.codemod import VisitorBasedCodemodCommand

from .renaming_definitions import (
    ATTR_TO_ATTR,
    METHOD_ARGS_TO_ARGS,
    METHOD_DEF,
    METHOD_TO_ATTR,
    METHOD_TO_METHOD,
    METHOD_WITH_ARGS_TO_ATTR,
)


def one_of_names(names: Iterable[str]) -> m.OneOf:
    return m.OneOf(*(m.Name(name) for name in names))


class AttributesRenamer(VisitorBasedCodemodCommand):
    def __create_attr_from_value(self, qual_name: str, value: cst.Attribute) -> cst.Attribute:
        for attr_atom in qual_name.split("."):
            value = cst.Attribute(value=value, attr=cst.Name(attr_atom))
        return value

    @singledispatchmethod
    def __create_attr_from_node(
        self,
        original_node: cst.Call | cst.Attribute,
        qual_name: str,
    ) -> cst.Attribute:
        raise NotImplementedError()

    @__create_attr_from_node.register
    def _(
        self,
        original_node: cst.Call,
        qual_name: str,
    ) -> cst.Attribute:
        children = original_node.children
        if isinstance(children[0], cst.LeftParen):
            # The node is surrounded by (...) for formatting purposes.
            child = children[1]
        else:
            child = children[0]
        return self.__create_attr_from_value(qual_name, child.value)

    @__create_attr_from_node.register
    def _(
        self,
        original_node: cst.Attribute,
        qual_name: str,
    ) -> cst.Attribute:
        return self.__create_attr_from_value(qual_name, original_node.value)

    @m.leave(m.Call(func=m.Attribute(attr=one_of_names(METHOD_TO_ATTR)), args=()))
    def leave_mta(self, original_node: cst.Call, updated_node: cst.Call) -> cst.Attribute:
        return self.__create_attr_from_node(original_node, METHOD_TO_ATTR[original_node.func.attr.value])

    @m.leave(m.Call(func=m.Attribute(attr=one_of_names(METHOD_WITH_ARGS_TO_ATTR)), args=[m.AtLeastN(n=1)]))
    def leave_mwata(self, original_node: cst.Call, updated_node: cst.Call) -> cst.Attribute | cst.Call:
        definition = METHOD_WITH_ARGS_TO_ATTR[original_node.func.attr.value]
        if not m.matches(original_node, m.Call(args=definition["args"])):
            return updated_node
        return self.__create_attr_from_node(original_node, definition["attr"])

    @m.leave(m.Call(func=m.Attribute(attr=one_of_names(METHOD_TO_METHOD))))
    def leave_mtm(self, original_node: cst.Call, updated_node: cst.Call) -> cst.Call:
        return updated_node.with_changes(
            func=self.__create_attr_from_node(original_node, METHOD_TO_METHOD[original_node.func.attr.value])
        )

    @m.leave(m.Attribute(attr=one_of_names(ATTR_TO_ATTR)))
    def leave_ata(self, original_node: cst.Attribute, updated_node: cst.Attribute) -> cst.Attribute:
        return self.__create_attr_from_node(original_node, ATTR_TO_ATTR[original_node.attr.value])

    @m.leave(m.Call(func=m.Attribute(attr=one_of_names(METHOD_ARGS_TO_ARGS)), args=[m.AtLeastN(n=1)]))
    def leave_mata(self, original_node: cst.Call, updated_node: cst.Call) -> cst.Call:
        new_to_old_args = METHOD_ARGS_TO_ARGS[original_node.func.attr.value]
        matcher = m.Call(
            args=[
                m.ZeroOrMore(),
                m.Arg(keyword=one_of_names(new_to_old_args)),
                m.ZeroOrMore(),
            ],
        )
        if not m.matches(original_node, matcher):
            return updated_node

        newargs = []
        for arg in updated_node.args:
            if arg.keyword is not None and (kw := new_to_old_args.get(arg.keyword.value)):
                newargs.append(arg.with_changes(keyword=cst.Name(kw)))
            else:
                newargs.append(arg)

        return updated_node.with_changes(args=newargs)

    @m.leave(m.FunctionDef(name=one_of_names(METHOD_ARGS_TO_ARGS)))
    def leave_dmata(self, original_node: cst.FunctionDef, updated_node: cst.FunctionDef) -> cst.FunctionDef:
        new_to_old_args = METHOD_ARGS_TO_ARGS[original_node.name.value]
        matcher = m.FunctionDef(
            params=m.Parameters(
                [
                    m.ZeroOrMore(),
                    m.Param(name=one_of_names(new_to_old_args)),
                    m.ZeroOrMore(),
                ]
            ),
        )
        if not m.matches(original_node, matcher):
            return updated_node

        new_params = []
        for param in updated_node.params.params:
            if kw := new_to_old_args.get(param.name.value):
                new_params.append(param.with_changes(name=cst.Name(kw)))
            else:
                new_params.append(param)

        return updated_node.with_deep_changes(updated_node.params, params=new_params)

    def leave_FunctionDef(self, original_node: cst.FunctionDef, updated_node: cst.FunctionDef) -> cst.FunctionDef:
        for matcher, new_params in METHOD_DEF.items():
            if m.matches(original_node, matcher):
                return updated_node.with_deep_changes(updated_node.params, params=new_params)
        return updated_node
