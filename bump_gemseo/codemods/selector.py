# Copyright (c) IRT Saint Exupery.
#
# This source code is licensed under the MIT license.
from enum import Enum

from libcst.codemod import ContextAwareTransformer
from libcst.codemod.visitors import AddImportsVisitor, RemoveImportsVisitor

from bump_gemseo.codemods.attributes_renamer import AttributesRenamer
from bump_gemseo.codemods.import_renamer import import_renamer_classes


class Rule(str, Enum):
    pass


def gather_codemods(disabled: list[Rule]) -> list[type[ContextAwareTransformer]]:
    codemods: list[type[ContextAwareTransformer]] = []

    codemods.extend(import_renamer_classes)
    codemods.append(AttributesRenamer)

    # Those codemods need to be the last ones.
    codemods.extend([RemoveImportsVisitor, AddImportsVisitor])
    return codemods
