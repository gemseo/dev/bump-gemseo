# Copyright (c) IRT Saint Exupery.
#
# This source code is licensed under the MIT license.
"""Create the classes for renaming imports."""

from bump_gemseo.codemods.base_import_renamer import BaseImportRenamer
from bump_gemseo.codemods.renaming_definitions import OLD_TO_NEW_IMPORTS

import_renamer_classes = []
for old_name, new_name in OLD_TO_NEW_IMPORTS.items():
    dict_ = {
        "OLD_NAME": old_name,
        "NEW_NAME": new_name,
    }
    class_name = f"Rename{old_name.replace('.', '_')}"
    cls = type(class_name, (BaseImportRenamer,), dict_)
    cls.__module__ = __name__
    globals()[class_name] = cls
    import_renamer_classes += [cls]
