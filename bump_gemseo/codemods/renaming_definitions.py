# Copyright (c) IRT Saint Exupery.
#
# This source code is licensed under the MIT license.
"""Definitions for renaming code."""

from __future__ import annotations
from libcst import FunctionDef, Name, Param, Parameters
import libcst.matchers as m

METHOD_TO_ATTR = {
    # MDODiscipline
    "get_input_data_names": "io.input_grammar.names",
    "get_output_data_names": "io.output_grammar.names",
    # DesignSpace
    "has_current_value": "has_current_value",
    "has_integer_variables": "has_integer_variables",
    # OptimizationProblem
    "get_feasible_points": "history.feasible_points",
    "get_scalar_constraint_names": "scalar_constraint_names",
}

METHOD_WITH_ARGS_TO_ATTR = {
    "get_input_data_names": {
        "args": [m.Arg(keyword=m.Name("with_namespace"), value=m.Name("False"))],
        "attr": "io.input_grammar.names_without_namespace",
    },
    "get_output_data_names": {
        "args": [m.Arg(keyword=m.Name("with_namespace"), value=m.Name("False"))],
        "attr": "io.output_grammar.names_without_namespace",
    },
}


METHOD_TO_METHOD = {
    # MDODiscipline
    "_filter_inputs": "prepare_input_data",
    "_filter_local_data": "io.data._filter_local_data",
    "is_all_inputs_existing": "io.input_grammar.has_names",
    "is_all_outputs_existing": "io.output_grammar.has_names",
    "set_cache_policy": "set_cache",
    "get_expected_dataflow": "workflow.get_expected_dataflow",
    "store_local_data": "io.update_output_data",
    # OptimizationProblem
    "check_design_point_is_feasible": "history.check_design_point_is_feasible",
    # "problem.objective": "problem.objective.evaluate",
    # "problem.dimension": "problem.design_space.dimension",
    # "get_optimum_from_database": "_get_optimum_from_database",
    "get_right_sign_constraints": "_get_right_sign_constraints",
    "get_x0_normalized": "design_space.get_current_value",
    "get_ineq_constraints": "constraints.get_inequality_constraints",
    "get_eq_constraints": "constraints.get_equality_constraints",
    "get_constraint_names": "constraints.get_names",
    "get_number_of_unsatisfied_constraints": "constraints.get_number_of_unsatisfied_constraints",
    "nonproc_objective": "objective.original",
    "add_callback": "add_listener",
    # DesignSpace
    "array_to_dict": "convert_array_to_dict",
    "dict_to_array": "convert_dict_to_array",
    # Formulations
    "get_top_level_disc": "get_top_level_disciplines",
    # Scenario
    "_run_algorithm": "_run",
}

# METHOD_TO_METHOD_WITH_ARGS = {
#     "add_ineq_constaints(xx)": {"name": "add_contraint", "kwargs": ("constraint_type=MDOFunction.ConstraintType.INEQ",)}
# }

ATTR_TO_ATTR = {
    # DriverLibrary
    "activate_progress_bar": "enable_progress_bar",
    # MDODiscipline
    "activate_input_data_check": "validate_input_data",
    "activate_output_data_check": "validate_output_data",
    "_local_data": "io.data",
    "local_data": "io.data",
    "data_processor": "io.data_processor",
    "grammar_type": "io.grammar_type",
    "n_calls": "execution_statistics.n_calls",
    "n_calls_linearize": "execution_statistics.n_calls_linearize",
    "default_inputs": "default_input_data",
    "default_outputs": "default_output_data",
    "_differentiated_inputs": "_differentiated_input_names",
    "_differentiated_outputs": "_differentiated_output_names",
    "_is_linearized": "_has_jacobian",
    "cache_hfd_file": "hdf_file_path",
    "cache_node_path": "hdf_node_path",
    # OptimizationProblem
    "NORMALIZE_DESIGN_SPACE_OPTION": "_NORMALIZE_DESIGN_SPACE",
    "MAX_ITER": "_MAX_ITER",
    "RESIDUALS_NORM": "NORMALIZED_RESIDUAL_NORM",
    "opt_problem": "optimization_problem",
    "eq_tolerance": "tolerances.equality",
    "ineq_tolerance": "tolerances.inequality",
    "current_iter": "evaluation_counter.current",
    "max_iter": "evaluation_counter.maximum",
    # Factory
    "_MODULE_NAMES": "_PACKAGE_NAMES",
}

# The method names shall be the new ones.
METHOD_ARGS_TO_ARGS = {
    # Discipline
    "add_differentiated_inputs": {
        "inputs": "input_names",
    },
    "add_differentiated_outputs": {
        "outputs": "output_names",
    },
    "check_jacobian": {
        "inputs": "input_names",
        "outputs": "output_names",
    },
    "_compute_jacobian": {
        "inputs": "input_names",
        "outputs": "output_names",
    },
    # DesignSpace
    "add_variable": {
        "u_b": "upper_bound",
        "l_b": "lower_bound",
        "var_type": "type_",
    },
    # Scenario
    "execute": {
        "algo": "algo_name",
    },
    # TODO: support function in addition to methods.
    "MDOScenario": {
        "formulation": "formulation_name",
        "inner_mda_name": "main_mda_name",
    },
    # UI
    "configure": {
        "activate_discipline_counters": "enable_discipline_statistics",
        "activate_function_counters": "enable_function_statistics",
        "activate_progress_bar": "enable_progress_bar",
        "activate_discipline_cache": "enable_discipline_cache",
        "check_input_data": "validate_input_data",
        "check_output_data": "validate_output_data",
    },
}

METHOD_DEF = {
    m.FunctionDef(
        name=m.Name("_run"),
        params=m.Parameters([m.Param(name=m.Name("self"))]),
    ): [Param(name=Name("self")), Param(name=Name("input_data"))],
}

OLD_TO_NEW_IMPORTS = {
    "gemseo.algos.doe.doe_library.DOELibrary": "gemseo.algos.doe.base_doe_library.BaseDOELibrary",
    "gemseo.algos.doe.doe_library": "gemseo.algos.doe.base_doe_library",
    "gemseo.algos.algorithm_library.AlgorithmLibrary": "gemseo.algos.base_algorithm_library.BaseAlgorithmLibrary",
    "gemseo.algos.driver_library.DriverLibrary": "gemseo.algos.base_driver_library.BaseDriverLibrary",
    "gemseo.algos.driver_library": "gemseo.algos.base_driver_library",
    "gemseo.algos.ode_solver_lib.ODESolverLibrary": "gemseo.algos.ode.base_ode_solver_library.BaseODESolverLibrary",
    "gemseo.algos.opt.optimization_library.OptimizationLibrary": "gemseo.algos.opt.base_optimization_library.BaseOptimizationLibrary",
    "gemseo.algos.opt.optimization_library": "gemseo.algos.opt.base_optimization_library",
    "gemseo.algos.opt_problem.max_iter": "gemseo.algos.optimization_problem.evaluation_counter.maximum",
    "gemseo.algos.opt_problem.current_iter": "gemseo.algos.optimization_problem.evaluation_counter.current",
    "gemseo.algos.opt_problem": "gemseo.algos.optimization_problem",
    "gemseo.algos.opt_result": "gemseo.algos.optimization_result",
    "gemseo.algos.opt_result_multiobj": "gemseo.algos.multiobjective_optimization_result",
    "gemseo.algos.pareto.ParetoFront": "gemseo.post.pareto_front.ParetoFront",
    "gemseo.algos.ode.ode_solvers_factory.ODESolversFactory": "gemseo.algos.ode.factory.ODESolverLibraryFactory",
    "gemseo.algos.ode.ode_solver_lib.ODESolverLib": "gemseo.algos.ode.base_ode_solver_library.BaseODESolverLibrary",
    "gemseo.algos.ode.ode_solver_lib": "gemseo.algos.ode.base_ode_solver_library",
    "gemseo.algos.linear_solvers.linear_solvers_factory.LinearSolversFactory": "gemseo.algos.linear_solvers.factory.LinearSolverLibraryFactory",
    "gemseo.algos.linear_solvers.linear_solver_library.LinearSolversLibrary": "gemseo.algos.linear_solvers.base_linear_solver_library.BaseLinearSolversLibrary",
    "gemseo.algos.linear_solvers.linear_solver_library": "gemseo.algos.linear_solvers.base_linear_solver_library",
    "gemseo.post.factory.OptPostProcessorFactory": "gemseo.post.factory.PostFactory",
    "gemseo.post.opt_post_processor.OptPostProcessor": "gemseo.post.base_post.BasePost",
    "gemseo.post.scatter_mat": "gemseo.post.scatter_plot_matrix",
    "gemseo.post.para_coord": "gemseo.post.parallel_coordinates",
    "gemseo.algos.opt.opt_factory.OptimizersFactory": "gemseo.algos.opt.factory.OptimizationLibraryFactory",
    "gemseo.disciplines.factory.MDODisciplineFactory": "gemseo.disciplines.factory.DisciplineFactory",
    "gemseo.formulations.mdo_formulation.MDOFormulation": "gemseo.formulations.base_mdo_formulation.BaseMDOFormulation",
    "gemseo.core.mdofunctions": "gemseo.core.mdo_functions",
    "gemseo.core.coupling_structure.MDOCouplingStructure": "gemseo.core.coupling_structure.CouplingStructure",
    "gemseo.problems.analytical.power_2.Power2": "gemseo.problems.optimization.power_2.Power2",
    "gemseo.core.grammars.pydantic_ndarray.NDArrayPydantic": "gemseo.utils.pydantic_ndarray.NDArrayPydantic",
    "gemseo.algos.doe.lib_custom": "gemseo.algos.doe.custom_doe.custom_doe",
    "gemseo.algos.doe.lib_openturns": "gemseo.algos.doe.openturns.openturns",
    "gemseo.core.discipline.MDODiscipline": "gemseo.core.discipline.discipline.Discipline",
    "gemseo.MDODiscipline": "gemseo.core.discipline.discipline.Discipline",
    "gemseo.core.chain.MDOChain": "gemseo.core.chains.chain.MDOChain",
    "gemseo.core.chain.MDOWarmStartedChain": "gemseo.core.chains.warm_started_chain.MDOWarmStartedChain",
    "gemseo.core.data_processor.DataProcessor": "gemseo.core.discipline.data_processor.DataProcessor",
}


# TODO:
# - to_pickle
# - from_pickle

# "get_x0_and_bounds": "get_value_and_bounds",
# from gemseo.algos.design_space_utils import get_value_and_bounds
# x_0, l_b, u_b = get_value_and_bounds(problem.design_space, False)
