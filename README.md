# Bump GEMSEO ♻️

[![PyPI - Version](https://img.shields.io/pypi/v/bump-gemseo.svg)](https://pypi.org/project/bump-gemseo)
[![PyPI - Python Version](https://img.shields.io/pypi/pyversions/bump-gemseo.svg)](https://pypi.org/project/bump-gemseo)


Bump GEMSEO is a tool to help you migrate your code from GEMSEO V5 to V6.
It is based on the tool [bump-pydantic](https://github.com/pydantic/bump-pydantic).


> [!NOTE]\
> If you find bugs, please report them on the [issue tracker](https://gitlab.com/gemseo/dev/bump-gemseo/issues).

---

## Installation

Install from the repository with

```bash
pip install bump-gemseo@git+https://gitlab.com/gemseo/dev/bump-gemseo.git@develop
```

You may also use `uv` or `pipx` to install it in isolation.

---

## Usage

`bump-gemseo` is a CLI tool, hence you can use it from your terminal.

It's easy to use. If your project structure is:

```bash
repository/
└── my_package/
    └── <python source files>
```

Then you'll want to do:

```bash
cd /path/to/repository
bump-gemseo my_package
```

### Check diff before applying changes

To check the diff before applying the changes, you can run:

```bash
bump-gemseo --diff <path>
```

### Apply changes

To apply the changes, you can run:

```bash
bump-gemseo <path>
```

<!-- ## Rules -->
<!--  -->
<!-- You can find below the list of rules that are applied by `bump-gemseo`. -->
<!--  -->
<!-- It's also possible to disable rules by using the `--disable` option. -->
<!--  -->
<!-- ### BP001: Add default `None` to `Optional[T]`, `Union[T, None]` and `Any` fields -->
<!--  -->
<!-- - ✅ Add default `None` to `Optional[T]` fields. -->
<!--  -->
<!-- The following code will be transformed: -->
<!--  -->
<!-- ```py -->
<!-- class User(BaseModel): -->
<!--     name: Optional[str] -->
<!-- ``` -->
<!--  -->
<!-- Into: -->
<!--  -->
<!-- ```py -->
<!-- class User(BaseModel): -->
<!--     name: Optional[str] = None -->
<!-- ``` -->

---

## License

This project is licensed under the terms of the MIT license.
